#include <iostream>
#include <stdint.h>

#define s16 int16_t
#define s32 int32_t

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))                                    

float prompt_float(const std::string &prompt) {
	std::string input;
	prompt:
	std::cin.clear();
	std::cout << prompt << " [float]: ";
	std::getline(std::cin, input);

	try {
		return std::stof(input);
	} catch (...) {
		std::cout << "err: invalid input." << std::endl;
		std::clearerr(stdin);
		goto prompt;
	}
}

void fresnel_params(s16* scale, s16* offset, float lo, float hi){
    s32 dotMin = (s32)(lo * 0x7FFF);
    s32 dotMax = (s32)(hi * 0x7FFF);
    if(dotMax == dotMin) ++dotMax;
    s32 scale32 = 0x3F8000 / (dotMax - dotMin);
    s32 offset32 = -(0x7F * dotMin) / (dotMax - dotMin);
    *scale = (s16)MAX(MIN(scale32, 0x7FFF), -0x8000);
    *offset = (s16)MAX(MIN(offset32, 0x7FFF), -0x8000);
}

int main(int argc, const char *argv[]) {
    s16 s, o;
    std::cout << "F3DEX3 Fresnel Calculator by red" << std::endl;
    fresnel_params(&s, &o, prompt_float("Enter value LO"), prompt_float("Enter value HI"));
    std::cout << "Your fresnel values are:" << std::endl << "Scale: " << std::to_string(s) << std::endl << "Offset: " << std::to_string(o) << std::endl;
}
