#ifndef _RGFX_RANDOM
#define _RGFX_RANDOM
u16 random_u16(void);
float random_float(void);
s32 random_sign(void);
u32 rgfx_random_xorshift(void);
#endif