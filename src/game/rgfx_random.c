#include <ultra64.h>
#include "types.h"
#include "sm64.h"

volatile u32 gRandomSeed32;

/*
 * We initialize the seed with the count register on CP0.
 * On real hardware, this ensures a different value each game boot in all circumstances.
 * Consequently, this destroys any chance at using a TASbot.
 */

void rgfx_init_count_seed() {
    #ifdef TASBOT_RANDOM
        gRandomSeed32 = 0;
    #else
        gRandomSeed32 = osGetCount();
    #endif
}

u32 rgfx_random_xorshift(void) {
    u32 x = gRandomSeed32;
    x ^= x << 13;
    x ^= x >> 7;
    x ^= x << 5;
    return gRandomSeed32 = x;
}
// Returns a u16 casted rgfx_random_xoshiro(). Just like the original vanilla implementation, this will return a random number from 0-65535 inclusive (functionally equivalent).

u16 random_u16(void) {
    return (u16)rgfx_random_xorshift();
}

#ifdef USING_GCC
ALWAYS_INLINE const float CF(u16 a) {
    IGNORE register u32 in asm("f10") = a << 16;
    register float out asm("f10");

    return out;
}
#endif


// Kaze's better version of random_float(). Functionally equivalent.

f32 random_float(void) {
#ifndef USING_GCC
    f32 rnd = random_u16();
    return rnd / (double) 0x10000;
#else
    f32 result;
    *(u32 *) &result = 0x3F800000 | (random_u16() << 7);
    return result - CF(0x3F80);
#endif
}

// Return either -1 or 1 with a 50:50 chance.

s32 random_sign(void) {
    if (random_u16() >= 0x7FFF) {
        return 1;
    }
    return -1;
}