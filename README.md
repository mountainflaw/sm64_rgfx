# RGFX SM64

***This repository at this current moment in time is experimental!***

This is based on the Shindou *Super Mario 64* release with the USA *Super Mario 64* text and assets. A ROM dump of both releases is required to build this repository.

Goals for this repository:

- Improved audio engine.
- Subtle graphics improvements.
- Extra graphical capabilities for developers.
- Minor-to-significant code optimizations and refactors (outsourced from other repositories).

This repository has the same build requirements as the vanilla *Super Mario 64* decompilation repository.

Join our discord! https://discord.gg/F5Hy9dBFbM

## Quick Start (for Ubuntu)

1. Install prerequisites: `sudo apt install -y binutils-mips-linux-gnu build-essential git pkgconf python3`
2. Clone the repo from within Linux: `git clone https://gitlab.com/mountainflaw/sm64_rgfx.git`
3. Place a Super Mario 64 ROM called `baserom.<VERSION>.z64` into the project folder for asset extraction, where `VERSION` for this repository is `us` and `sh`.
4. Run the `init_setup.sh` script in the repository's root directory to correctly extract the assets.
4. Run `make` to build. Specify the version through `make`. Add `-j4` to improve build speed (hardware dependent).

Ensure the repo path length does not exceed 255 characters. Long path names result in build errors.

## Installation

### Windows

Install WSL and a distro of your choice following
[Windows Subsystem for Linux Installation Guide for Windows 10.](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
We recommend either Debian or Ubuntu 18.04 Linux distributions under WSL.
Note: WSL1 does not currently support Ubuntu 20.04.

Next, clone the SM64 repo from within the Linux shell:
`git clone https://gitlab.com/mountainflaw/sm64_rgfx.git`

Then continue following the directions in the [Linux](#linux) installation section below.

### Linux

There are 3 steps to set up a working build.

#### Step 1: Install dependencies

The build system has the following package requirements:
 * binutils-mips
 * pkgconf
 * python3 >= 3.6

Dependency installation instructions for common Linux distros are provided below:

##### Debian / Ubuntu
To install build dependencies:
```
sudo apt install -y binutils-mips-linux-gnu build-essential git pkgconf python3
```

##### Arch Linux
To install build dependencies:
```
sudo pacman -S base-devel python
```
Install the following AUR packages:
* [mips64-elf-binutils](https://aur.archlinux.org/packages/mips64-elf-binutils) (AUR)

##### Other Linux distributions

Most modern Linux distributions should have equivalent packages to the other two listed above.
You may have to use a different version of GNU binutils. Listed below are fully compatible binutils
distributions with support in the makefile, and examples of distros that offer them:

* `mips64-elf-` (Arch AUR)
* `mips-linux-gnu-` (Ubuntu and other Debian-based distros)
* `mips64-linux-gnu-` (RHEL/CentOS/Fedora)

You may also use [Docker](#docker-installation) to handle installing an image with minimal dependencies.

#### Step 2: Copy baserom(s) for asset extraction

For each version (us and sh) for which you want to build a ROM, put an existing ROM at
`./baserom.<VERSION>.z64` for asset extraction.

##### Step 3: Build the ROM

Run `make` to build the ROM.
Other examples:
```
make -j4 # build (J) version instead with 4 jobs
```

Resulting artifacts can be found in the `build` directory.

The full list of configurable variables are listed below, with the default being the first listed:

* ``GRUCODE``: ``f3d_old``, ``f3d_new``, ``f3dex``, ``f3dex2``, ``f3dzex``
* ``CROSS``: Cross-compiler tool prefix (Example: ``mips64-elf-``).

*It is recommended that you stick to the default `f3dzex` grucode, as it will likely become mandatory down the line.*
*Please do not change the VERSION flag. Only English-release assets with Shindou-release code is supported.*

## Project Structure

	sm64
	├── actors: object behaviors, geo layout, and display lists
	├── asm: handwritten assembly code, rom header
	│   └── non_matchings: asm for non-matching sections
	├── assets: animation and demo data
	│   ├── anims: animation data
	│   └── demos: demo data
	├── bin: C files for ordering display lists and textures
	├── build: output directory
	├── data: behavior scripts, misc. data
	├── doxygen: documentation infrastructure
	├── enhancements: example source modifications
	├── include: header files
	├── levels: level scripts, geo layout, and display lists
	├── lib: SDK library code
	├── rsp: audio and Fast3D RSP assembly code
	├── sound: sequences, sound samples, and sound banks
	├── src: C source code for game
	│   ├── audio: audio code
	│   ├── buffers: stacks, heaps, and task buffers
	│   ├── engine: script processing engines and utils
	│   ├── game: behaviors and rest of game source
	│   ├── goddard: Mario intro screen
	│   └── menu: title screen and file, act, and debug level selection menus
	├── text: dialog, level names, act names
	├── textures: skybox and generic texture data
	└── tools: build tools

## Contributing

Pull requests are welcome. For major changes, please open an issue first to
discuss what you would like to change.

Run `clang-format` on your code to ensure it meets the project's coding standards.

### Credits

 * red/bicycle soda/mountainflaw: RGFX graphics stack (HUD library, skybox system, environment system)
 * HackerSM64 team: Matstack fix, fastprint
 * Sauraen: F3DEX3 microcode
 * ArcticJaguar725/Gheskett: NEOS (sound engine) code advice
 * SM64 decomp team: Base decomp
